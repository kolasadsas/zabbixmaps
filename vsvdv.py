import csv


from pyzabbix import ZabbixAPI

with open('Devices1.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    hosts = []
    maps = {}
    for line in reader:
        m = line['Maps']
        h = line['Name']
        for i in ['(', ')', '/', '#', '+']:
            h = h.replace(i, '_')

        a = line['Addresses']
        if m not in maps.keys():
            maps[m] = []
        maps[m].append({
            'name': h, 'ip': a
        })

        from pprint import pprint

        pprint(m)