import csv

from pyzabbix import ZabbixAPI, ZabbixAPIException

SERVER_ZABBIX = ''
LOGIN_ZABBIX = ''
PASSWORD_ZABBIX = ''

z = ZabbixAPI(SERVER_ZABBIX)
z.login(user=LOGIN_ZABBIX, password=PASSWORD_ZABBIX)

group = "41"

with open('Devices1.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    hosts = []
    for raw in reader:
        name = raw['Name']
        address = raw['Addresses']
        maps = raw['Maps']
        if 'Proletarska' in maps:
            for i in ['(', ')', '/', '#', '+']:
                name = name.replace(i, '_')
            response = z.do_request(method="host.create", params={

                "host": name,

                "interfaces": [

                    {

                        "type": 1,

                        "main": 1,

                        "useip": 1,

                        "ip": raw['Addresses'],

                        "dns": "",

                        "port": "10050"

                    }

                ],

                "groups": [

                    {

                        "groupid": ""

                    }

                ],

            }
                                    )
            hosts.append((response['result']['hostids'][0]))

    params = {"name": "Host map", "width": 1600, "height": 1280}

    count = 1
    x = 50
    y = 50

    selements = []
    for id in hosts:
        selements.append(
            {
                "elementid": id,
                "selementid": str(count),
                "elementtype": 0,
                "iconid_off": "102",
                "x": x,
                "y": y
            }
        )
        count += 1
        x = x + 300 if x <= 1300 else 300
        y = y + 50 if x == 300 else y


    params['selements'] = selements

    try:
        z.do_request(method="map.create", params=params)
        print("zbs")
    except ZabbixAPIException:
        ze = ZabbixAPIException('this map is already exist')
        print(ze, 'ze')
