import csv
import re

from pyzabbix import ZabbixAPI, ZabbixAPIException

SERVER_ZABBIX = 'http://192.168.33.65/zabbix'
LOGIN_ZABBIX = 'kolas'
PASSWORD_ZABBIX = ''

z = ZabbixAPI(SERVER_ZABBIX)
z.login(user=LOGIN_ZABBIX, password=PASSWORD_ZABBIX)

inputstr = ''

with open('Devices1.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    maps = {}
    for line in reader:
        mp = line['Maps']
        host = re.sub('[^A-Za-z0-9_\-. ]', '', line['Name'])

        ip = line['Addresses']
        try:
            maps[mp]
        except KeyError:
            maps[mp] = []

        response = z.do_request(method="host.create", params={
            "host": host,
            "interfaces": [{"type": 1, "main": 1, "useip": 1, "ip": ip, "dns": "", "port": "10050"}],
            "groups": [{"groupid": "41"}],
        })

        maps[mp].append(response['result']['hostids'][0])  # list of ids

    for mp, ids in maps.items():

        params = {"name": mp, "width": 1600, "height": 1280}

        count = 1
        x = y = 50

        selements = []
        for id in ids:
            selements.append(
                {
                    "elementid": id,
                    "selementid": str(count),
                    "elementtype": 0,
                    "iconid_off": "102",
                    "x": x,
                    "y": y
                }
            )
            count += 1
            x = x + 300 if x <= 1300 else 50
            y = y + 50 if x == 50 else y

        params['selements'] = selements

        try:
            z.do_request(method="map.create", params=params)
            print("zbs")
        except ZabbixAPIException:
            ze = ZabbixAPIException('this map is already exist')
            print(ze)

        if inputstr != 'ok':
            inputstr = input("press Enter")
